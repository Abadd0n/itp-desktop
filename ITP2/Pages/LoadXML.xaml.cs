﻿using ITP2.Model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.Serialization;

namespace ITP2.Pages
{
    /// <summary>
    /// Logika interakcji dla klasy LoadXML.xaml
    /// </summary>
    public partial class LoadXML : Page
    {
        List<Table> list = new List<Table>();
        public LoadXML()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            
            XmlSerializer serializer = new XmlSerializer(typeof(List<Table>), 
                new XmlRootAttribute("KursyWalut"));
            
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "XML-File | *.xml";

            bool? result = openFileDialog.ShowDialog();

            if(result == true)
            {
                using (FileStream fs = new FileStream(openFileDialog.FileName, FileMode.Open, FileAccess.Read))
                {
                    list = serializer.Deserialize(fs) as List<Table>;
                }

                this.dataGrid.ItemsSource = list;
            }
        }
    }
}
