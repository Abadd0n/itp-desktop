﻿using CsvHelper;
using ITP2.Model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ITP2.Pages
{
    /// <summary>
    /// Logika interakcji dla klasy GenerateCSV.xaml
    /// </summary>
    public partial class GenerateCSV : Page
    {
        List<Adress> adresses = new List<Adress>();
        List<TextBox> textBoxes = new List<TextBox>();

        public GenerateCSV()
        {
            InitializeComponent();
            textBoxes.Add(textBox);
            textBoxes.Add(textBox1);
            textBoxes.Add(textBox2);
            textBoxes.Add(textBox3);
            textBoxes.Add(textBox4);
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            adresses.Add(new Adress(){ Name = textBox.Text, Surname = textBox1.Text, 
                PostalCode = textBox2.Text, City = textBox3.Text, Street = textBox4.Text });

            foreach(var t in textBoxes)
            {
                t.Clear();
            }

            this.dataGrid.ItemsSource = null;
            this.dataGrid.ItemsSource = adresses;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "CSV-File | *.csv";

            bool? result = saveFileDialog.ShowDialog();

            if(result == true)
            {
                using(StreamWriter sw = new StreamWriter(saveFileDialog.FileName))
                {
                    using (var csv = new CsvWriter(sw, 
                        System.Globalization.CultureInfo.CreateSpecificCulture("pl-PL")))
                    {
                        csv.Configuration.HasHeaderRecord = false;
                        csv.Configuration.Delimiter = ";";
                        csv.WriteRecords(adresses);
                    }
                }
            }

            MessageBox.Show("Saved.");
        }
    }
}
