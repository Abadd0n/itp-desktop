﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using ITP2.Model;
namespace ITP2.Pages
{
    /// <summary>
    /// Logika interakcji dla klasy GenerateXML.xaml
    /// </summary>
    public partial class GenerateXML : Page
    {
        List<Table> list = new List<Table>();
        List<TextBox> textBoxes = new List<TextBox>();

        public GenerateXML()
        {
            InitializeComponent();
            textBoxes.Add(textBox);
            textBoxes.Add(textBox1);
            textBoxes.Add(textBox2);
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<Table>), new XmlRootAttribute("KursyWalut"));
            
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "XML-File | *.xml";
            
            bool? result = saveFileDialog.ShowDialog();

            if(result == true)
            {
                using(FileStream fs = new FileStream(saveFileDialog.FileName, FileMode.Create, FileAccess.Write))
                {
                    serializer.Serialize(fs, list);
                }
            }

            MessageBox.Show("Saved.");
        }

        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            list.Add(new Table() { Symbol = textBox.Text, Currency = textBox1.Text, Course = textBox2.Text});
            
            foreach(var t in textBoxes)
            {
                t.Clear();
            }
            
            MessageBox.Show("Added to list.");
        }
    }
}
