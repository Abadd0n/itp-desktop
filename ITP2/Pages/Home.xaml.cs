﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ITP2.Pages;

namespace ITP2
{
    /// <summary>
    /// Logika interakcji dla klasy Home.xaml
    /// </summary>
    public partial class Home : Page
    {
        public Home()
        {
            InitializeComponent();
        }

        private void OnClickLoadXMLFilePage(object sender, RoutedEventArgs e)
        {
             LoadXML goToLoad = new LoadXML();
             NavigationService.Navigate(goToLoad);
        }

        private void OnClickGenerateXMLFilePage(object sender, RoutedEventArgs e)
        {
            GenerateXML goToGenX = new GenerateXML();
            NavigationService.Navigate(goToGenX);
        }

        private void OnClickGenerateCSVFilePage(object sender, RoutedEventArgs e)
        {
            GenerateCSV goToGenC = new GenerateCSV();
            NavigationService.Navigate(goToGenC);
        }
    }
}
