﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ITP2.Model
{
    public class Table
    {
        private string symText, curText, couText;
        [XmlElement(ElementName = "SYMBOL")]
        public string Symbol { get; set; }
        
        [XmlElement(ElementName = "WALUTA")]
        public string Currency { get; set; }
        
        [XmlElement(ElementName = "KURS")]
        public string Course { get; set; }
    }
}
